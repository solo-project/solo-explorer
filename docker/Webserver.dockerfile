FROM nginx:1.13-alpine

ADD ./docker/nginx.conf /etc/nginx/conf.d/default.conf
ADD ./docker/explorer.minesolo.com.crt /etc/ssl/explorer.minesolo.com.crt
ADD ./docker/explorer.minesolo.com.key /etc/ssl/explorer.minesolo.com.key
ADD ./src/ /var/www/html
RUN chown -R nginx /var/www/html
